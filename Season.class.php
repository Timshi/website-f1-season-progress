<?php
    include_once("Race.class.php");

    class Season {

        private $name;
        private $settings;
        private $players = [];
        private $races = []; // Key = Driver name, Value = [0] Driver, [1] = Points / TODO: maybe replace key value array with own class
        private $driverStandings = []; // Key = Driver name, Value = [0] Driver, [1] Points, [2] = count races
        private $teamStandings = []; // Key = Team name, Value = [0] Driver, [1] Points

        public function __construct($name, $settings, $players, $races) {
            $this->name = $name;
            $this->settings = $settings;
            $this->players = $players;
            $this->races = $races;
            $this->updateStandings();
        }

        public function printAsTable() {
            $maxRacesCount = $this->settings->getMaxRacesCount();
            $racesCompletedCount = sizeof($this->races);
            $isFinished = $racesCompletedCount == $maxRacesCount;
            $progressIndicatorHtml = 
                $isFinished ? 
                "<img id=\"finished-indicator\" src=\"../img/finished.png\" alt=\"finished\"></h1>" : 
                "<img id=\"live-indicator\" src=\"../img/live.gif\" alt=\"live\"></h1>";

            echo "<section>";
            echo "<h1>$this->name $progressIndicatorHtml";
            echo "<p>" . implode(", ", $this->players) . "</p>";

            echo "<div id=\"flag-link-container\">";
            foreach ($this->races as $race) {
                $raceId = strtolower($race->getId());
                echo "
                <a class=\"flag-link\" href=\"#$raceId\">
                    <img src=\"../img/$raceId.png\" alt=\"$raceId flag\">
                </a>";
            }
            echo "</div>";

            $qualifying = $this->settings->getQualifying();
            $raceDistance = $this->settings->getRaceDistance();
            $carPerformance = $this->settings->getCarPerformance();
            echo "
            <div id=\"settings-container\">
                <div class=\"settings-item\">
                    <h3>Progress</h3>
                    <p>" . $racesCompletedCount . " / " . $maxRacesCount . "</p>
                </div>
                <div class=\"settings-item\">
                    <h3>Qualifying</h3>
                    <p>$qualifying</p>
                </div>
                <div class=\"settings-item\">
                    <h3>Race distance</h3>
                    <p>$raceDistance</p>
                </div>
                <div class=\"settings-item\">
                    <h3>Car performance</h3>
                    <p>$carPerformance</p>
                </div>
            </div>";

            echo $this->printDriverStandingsAsTable();
            echo "</section>";

            echo "<section>";
            echo $this->printTeamStandingsAsTable();
            echo "</section>";

            for ($i = 0; $i < sizeof($this->races); $i++) { 
                $this->races[$i]->printAsTable($i + 1);
            }
        }

        private function updateStandings() {
            foreach ($this->races as $race) {   
                if (sizeof($this->driverStandings) <= 0) {
                    // Set the initial values for the driver
                    $this->driverStandings = $race->getResultWithPoints();

                    // Set race count initially to one
                    foreach ($this->driverStandings as $result) {
                        $this->driverStandings[$result[0]->getId()] = [ $result[0], $result[1], 1 ];
                    }
                    
                } else if (sizeof($this->driverStandings) > 0) {
                    
                    // Update points of driver standings
                    foreach($race->getResultWithPoints() as $result) {

                        // Check if the driver id is already existing or new (e.g. joined later to season / switches team)
                        if (array_key_exists($result[0]->getId(), $this->driverStandings)) {
                            $this->driverStandings[$result[0]->getId()][1] += $result[1];   // sum up points
                            $this->driverStandings[$result[0]->getId()][2] += 1;            // sum up races count
                        } else {
                            $this->driverStandings[$result[0]->getId()] = [ $result[0], $result[1], 1 ];
                        }
                    }
                }

                // Update team standings
                foreach($race->getResultWithPoints() as $result) {
                    $teamName = $result[0]->getTeam()->getName();
                    if (array_key_exists($teamName, $this->teamStandings)) {
                        $this->teamStandings[$teamName][1] += $result[1];
                    } else {
                        $this->teamStandings[$teamName] = [ $result[0], $result[1] ];
                    }
                }
            }

            // Sort the standing arrays by points
            usort($this->driverStandings, array($this, "compareStandings"));
            usort($this->teamStandings, array($this, "compareStandings"));
        }

        private function printDriverStandingsAsTable() {
            $html = "<h2>Driver Standings</h2>";
            $html .= "
                <table>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Team</th>
                        <th class=\"number-column centered-column\">Races</th>
                        <th class=\"number-column centered-column\">Points</th>
                        <th class=\"number-column centered-column\">PPR</th>
                    </tr>
            ";
            
            $place = 1;
            foreach ($this->driverStandings as $standing) {
                $driver = $standing[0];

                // Create team badge
                $team = "<div class=\"" . $driver->getTeam()->getCssClass() . "\"></div>";

                $tmp = "<tr>";
                $tmp .= "<td>" . $place . "</td>";
                $tmp .= "<td>" . $driver->getName() . "</td>";
                $tmp .= "<td>" . $team . "<p>" . $driver->getTeam()->getName() . "</p> </td>";
                $tmp .= "<td class=\"number-column centered-column\">" . $standing[2] . "</td>";
                $tmp .= "<td class=\"number-column centered-column\">" . $standing[1] . "</td>";
                $tmp .= "<td class=\"number-column centered-column\">" . round($standing[1] / $standing[2], 0) . "</td>";
                $tmp .= "</tr>";
                $place++;

                if ($driver->getIsPlayer()) {
                    $tmp = str_replace("<td>", "<td class=\"player\">", $tmp);
                    $tmp = str_replace("<td class=\"number-column centered-column\">", "<td class=\"number-column centered-column player\">", $tmp);
                }

                $html .= $tmp;
            }

            $html .= "</table>";

            echo $html;
        }

        private function printTeamStandingsAsTable() {
            $html = "<h2>Team Standings</h2>";
            $html .= "
                <table>
                    <tr>
                        <th>#</th>
                        <th>Team</th>
                        <th class=\"centered-column\">Points</th>
                    </tr>
            ";
            
            $place = 1;
            foreach ($this->teamStandings as $standing) {

                // Create team badge
                $team = "<div class=\"" . $standing[0]->getTeam()->getCssClass() . "\"></div>";

                $html .= "<tr>";
                $html .= "<td>" . $place . "</td>";
                $html .= "<td>" . $team . "<p>" . $standing[0]->getTeam()->getName() . "</p> </td>";
                $html .= "<td class=\"centered-column\">" . $standing[1] . "</td>";
                $html .= "</tr>";
                $place++;
            }

            $html .= "</table>";

            echo $html;
        }

        private function compareStandings($item1, $item2) {
            if ($item1[1] == $item2[1]) return 0;
            return $item1[1] > $item2[1] ? -1 : 1;
        }
    }