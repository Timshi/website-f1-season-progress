function onScroll() {
	var jumpTopBtn = document.getElementById('go-top');
	
	if (document.documentElement.scrollTop == 0) {
		jumpTopBtn.style.display = "none";
	} 
	else {
		jumpTopBtn.style.removeProperty('display');
	}
}

function scrollToTop(sender) {
	document.body.scrollTop = 0; // For Safari
	document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
