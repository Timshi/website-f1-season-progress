<?php
    include_once("../Team.class.php");
    include_once("../Driver.class.php");

    // TEAMS 2020
    $MERCEDES = new Team("Mercedes", "mercedes20");
    $REDBULL = new Team("Red Bull", "red-bull20");
    $RACINGPOINT = new Team("Racing Point", "racing-point20");
    $MCLAREN = new Team("McLaren", "mclaren20");
    $FERRARI = new Team("Ferrari", "ferrari20");
    $RENAULT = new Team("Renault", "renault20");
    $ALPHATAURI = new Team("Alpha Tauri", "alpha-tauri20");
    $ALFAROMEO = new Team("Alfa Romeo", "alfa-romeo20");
    $HAAS = new Team("Haas", "haas20");
    $WILLIAMS = new Team("Williams", "williams20");

    // DRIVERS 2020 (BOTS)
    $HAM = new Driver("HAM", "Hamilton", $MERCEDES);
    $BOT = new Driver("BOT", "Bottas", $MERCEDES);
    $VET = new Driver("VET", "Vettel", $FERRARI);
    $LEC = new Driver("LEC", "Leclerc", $FERRARI);
    $VER = new Driver("VER", "Verstappen", $REDBULL);
    $ALB = new Driver("ALB", "Albon", $REDBULL);
    $PER = new Driver("PER", "Perez", $RACINGPOINT);
    $STR = new Driver("STR", "Stroll", $RACINGPOINT);
    $NOR = new Driver("NOR", "Norris", $MCLAREN);
    $SAI = new Driver("SAI", "Sainz", $MCLAREN);
    $RIC = new Driver("RIC", "Ricciardo", $RENAULT);
    $OCO = new Driver("OCO", "Ocon", $RENAULT);
    $GAS = new Driver("GAS", "Gasly", $ALPHATAURI);
    $KVY = new Driver("KVY", "Kvyat", $ALPHATAURI);
    $MAG = new Driver("MAG", "Magnussen", $HAAS);
    $GRO = new Driver("GRO", "Grosjean", $HAAS);
    $RAI = new Driver("RAI", "Räikkönen", $ALFAROMEO);
    $GIO = new Driver("GIO", "Giovanazzi", $ALFAROMEO);
    $RUS = new Driver("RUS", "Russel", $WILLIAMS);
    $LAT = new Driver("LAT", "Latifi", $WILLIAMS);

    // DRIVERS (PLAYERS)
    $TIM_RACINGPOINT = new Driver("TIM_RACINGPOINT", "Timshi", $RACINGPOINT, true);
    $TIM_RENAULT = new Driver("TIM_RENAULT", "Timshi", $RENAULT, true);
    $TIM_WILLIAMS = new Driver("TIM_WILLIAMS", "Timshi", $WILLIAMS, true);
    $TIM_ALPHATAURI = new Driver("TIM_WILLIAMS", "Timshi", $ALPHATAURI, true);
    $TIM_FERRARI = new Driver("TIM_FERRARI", "Timshi", $FERRARI, true);

    $BEATS_REDBULL = new Driver("BEATS_REDBULL", "Beats", $REDBULL, true);
    $BEATS_RACINGPOINT = new Driver("BEATS_RACINGPOINT", "Beats", $RACINGPOINT, true);
    $BEATS_RENAULT = new Driver("BEATS_RENAULT", "Beats", $RENAULT, true);
    $BEATS_MCLAREN = new Driver("BEATS_MCLAREN", "Beats", $MCLAREN, true);
    $BEATS_FERRARI = new Driver("BEATS_FERRARI", "Beats ", $FERRARI, true);
    $BEATS_MERCEDES = new Driver("BEATS_MERCEDES", "Beats", $MERCEDES, true);

    $MAGUSCH_FERRARI = new Driver("MAGUSCH_FERRARI", "mpw", $FERRARI, true);
    $MAGUSCH_REDBULL = new Driver("MAGUSCH_REDBULL", "mpw", $REDBULL, true);

    $SEAL_RACINGPOINT = new Driver("SEAL_RACINGPOINT", "Shyzune", $RACINGPOINT, true);
    $SEAL_MERCEDES = new Driver("SEAL_MERCEDES", "Shyzune", $MERCEDES, true);
    $SEAL_WILLIAMS = new Driver("SEAL_WILLIAMS", "Shyzune", $WILLIAMS, true);
    $SEAL_REDBULL = new Driver("SEAL_REDBULL", "Shyzune", $REDBULL, true);
    $SEAL_MCLAREN = new Driver("SEAL_MCLAREN", "Shyzune", $MCLAREN, true);
    $SEAL_RENAULT = new Driver("SEAL_RENAULT", "Shyzune", $RENAULT, true);

    $DIRK_REDBULL = new Driver("DIRK_REDBULL", "Folivora", $REDBULL, true);

    $TIMBO_REDBULL = new Driver("TIMBO_REDBULL", "Timbo", $REDBULL, true);
    $TIMBO_FERRARI = new Driver("TIMBO_FERRARI", "Timbo", $FERRARI, true);

    $SHERLOCK_MERCEDES = new Driver("SHERLOCK_MERCEDES", "Sherlock", $MERCEDES, true);
    $SHERLOCK_REDBULL = new Driver("SHERLOCK_REDBULL", "Sherlock", $REDBULL, true);
    $SHERLOCK_RACINGPOINT = new Driver("SHERLOCK_RACINGPOINT", "Sherlock", $RACINGPOINT, true);
    $SHERLOCK_FERRARI = new Driver("SHERLOCK_FERRARI", "Sherlock", $FERRARI, true);
    
    $FELIX_ALPHATAURI = new Driver("FELIX_ALPHATAURI", "Felix", $ALPHATAURI, true);
	
    $SPOOKI_HAAS = new Driver("SPOOKI_HAAS", "Spooki11", $HAAS, true);
	
	$MAX_MCLAREN = new Driver("MAX_MCLAREN", "Max", $MCLAREN, true);
	
	$MANU_REDBULL = new Driver("MANU_REDBULL", "ManuLJ", $REDBULL, true);
	
	$MENDE_REDBULL = new Driver("MENDE_REDBULL", "Mende", $REDBULL, true);
	
	$OSNATEL_MERCEDES = new Driver("OSNATEL_MERCEDES", "Osnatel", $MERCEDES, true);
	
    // MAIN SEASON TEAMS & PLAYERS - Possible workaround to split driver and AI teams
    // $FERRARI_PLAYERS = new Team("Ferrari (Spieler)", "ferrari");
    // $REDBULL_PLAYERS = new Team("Red Bull (Spieler)", "red-bull");
    // $RACINGPOINT_PLAYERS = new Team("Racing Point (Spieler)", "racing-point");
    // $ALPHATAURI_PLAYERS = new Team("Alpha Tauri (Spieler)", "alpha-tauri");
    // $MERCEDES_PLAYERS = new Team("Mercedes (Spieler)", "mercedes");

    // $TIM_RACINGPOINT_PLAYERS = new Driver("Timshi", $RACINGPOINT_PLAYERS, true);
    // $SEAL_RACINGPOINT_PLAYERS = new Driver("Shyzune", $RACINGPOINT_PLAYERS, true);
    // $SHERLOCK_FERRARI_PLAYERS = new Driver("Sherlock", $FERRARI_PLAYERS, true);
    // $BEATS_FERRARI_PLAYERS = new Driver("Beats ", $FERRARI_PLAYERS, true);
    // $BEATS_MERCEDES_PLAYERS = new Driver("Beats", $MERCEDES_PLAYERS, true);
    // $TIMBO_FERRARI_PLAYERS = new Driver("Timbo", $FERRARI_PLAYERS, true);
    // $DIRK_REDBULL_PLAYERS = new Driver("Folivora", $REDBULL_PLAYERS, true);
    // $MAGUSCH_REDBULL_PLAYERS = new Driver("mpw", $REDBULL_PLAYERS, true);
    // $FELIX_ALPHATAURI_PLAYERS = new Driver("Felix", $ALPHATAURI_PLAYERS, true);
?>