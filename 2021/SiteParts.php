<?php
    function printHeader($title) {
        echo "
        <head>
            <link rel=\"stylesheet\" href=\"../styles.css\">
            <link rel=\"stylesheet\" href=\"f12021styles.css\">
            <link rel=\"stylesheet\" href=\"../open-sans.css\">
            <script src=\"../scripts.js\"></script>
            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <title>F1 2021 - $title</title>
        </head>";
    }

    function printNavbar() {
        echo "
        <label id=\"label-toggle-menu\" for=\"toggle-menu\">
        <a href=\"../index.php\">
            <img id=\"logo\" src=\"../img/f1_logo21.png\" alt=\"logo\">
        </a>
            Menu
        </label>
        <input id=\"toggle-menu\" type=\"checkbox\">
        <nav>
            <ul>
                <li>
                    <a id=\"logo-link\" href=\"../index.php\">
                        <img id=\"logo\" src=\"../img/f1_logo21.png\" alt=\"logo\">
                    </a>
                </li>
                <li>
                    <a href=\"season1.php\">Main Season</a>
                </li>
            </ul>
        </nav>";
    }

    function printFooter() {
        echo "
            <footer>
                <input id=\"go-top\" type=\"button\" value=\"Go to top\" onclick=\"scrollToTop()\">
                <p>Made with ♥ by AzubiExcellence</p>
            </footer>
        ";
    }
?>
