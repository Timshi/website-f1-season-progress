<?php
    include_once("../Team.class.php");
    include_once("../Driver.class.php");

    // TEAMS 2020
    $MERCEDES = new Team("Mercedes", "mercedes21");
    $REDBULL = new Team("Red Bull", "red-bull21");
    $ASTONMARTIN = new Team("Aston Martin", "aston-martin21");
    $MCLAREN = new Team("McLaren", "mclaren21");
    $FERRARI = new Team("Ferrari", "ferrari21");
    $ALPINE = new Team("Apine", "alpine21");
    $ALPHATAURI = new Team("Alpha Tauri", "alpha-tauri21");
    $ALFAROMEO = new Team("Alfa Romeo", "alfa-romeo21");
    $HAAS = new Team("Haas", "haas21");
    $WILLIAMS = new Team("Williams", "williams21");

    // DRIVERS 2020 (BOTS)
    $HAM = new Driver("HAM", "Hamilton", $MERCEDES);
    $BOT = new Driver("BOT", "Bottas", $MERCEDES);
    $SAI = new Driver("SAI", "Sainz", $FERRARI);
    $LEC = new Driver("LEC", "Leclerc", $FERRARI);
    $VER = new Driver("VER", "Verstappen", $REDBULL);
    $PER = new Driver("PER", "Perez", $REDBULL);
    $VET = new Driver("VET", "Vettel", $ASTONMARTIN);
    $STR = new Driver("STR", "Stroll", $ASTONMARTIN);
    $NOR = new Driver("NOR", "Norris", $MCLAREN);
    $RIC = new Driver("RIC", "Ricciardo", $MCLAREN);
    $ALO = new Driver("ALO", "Alonso", $ALPINE);
    $OCO = new Driver("OCO", "Ocon", $ALPINE);
    $GAS = new Driver("GAS", "Gasly", $ALPHATAURI);
    $TSU = new Driver("TSU", "Tsunoda", $ALPHATAURI);
    $MSC = new Driver("MSC", "Schumacher", $HAAS);
    $MAZ = new Driver("MAZ", "Mazepin", $HAAS);
    $RAI = new Driver("RAI", "Räikkönen", $ALFAROMEO);
    $GIO = new Driver("GIO", "Giovanazzi", $ALFAROMEO);
    $RUS = new Driver("RUS", "Russel", $WILLIAMS);
    $LAT = new Driver("LAT", "Latifi", $WILLIAMS);

    // DRIVERS (PLAYERS)
    $TIM_MERCEDES = new Driver("Timshi", $MERCEDES, true);
?>