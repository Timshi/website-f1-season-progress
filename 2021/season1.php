<!DOCTYPE html>
<html lang="en">
<?php
    include_once("SiteParts.php");
    $seasonName = "Main Season";
    printHeader($seasonName);
?>
<body onload="onScroll()" onscroll="onScroll()">
    <?php printNavbar(); ?>

    <main>
        <?php
            include_once("Definitions.php");
            include_once("../Race.class.php");
            include_once("../Season.class.php");
            include_once("../SeasonSettings.class.php");

            // $race1 = new Race(
            //     Race::,
            //     ,
            //     array(
            //     )
            // );
            
            echo "<h2 style=\"text-align: center\">Starting soon :)</h2>";

            $settings = new SeasonSettings(SeasonSettings::$QUALI_FULL, SeasonSettings::$RACEDISTANCE_50, SeasonSettings::$CARPERFORMANCE_EQUAL, 22);
            $season = new Season($seasonName, $settings, [ "" ], array());
            //$season->printAsTable();
        ?>
    </main>

    <!-- <?php printFooter(); ?> -->
</body>
</html>