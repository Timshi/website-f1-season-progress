<?php
    class Team {

        private $name;
        private $cssClass;

        public function __construct($name, $cssClass) {
            $this->name = $name;
            $this->cssClass = $cssClass;
        }

        public function getName() {
            return $this->name;
        }

        public function getCssClass() {
            return $this->cssClass;
        }
    }