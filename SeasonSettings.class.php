<?php
    class SeasonSettings {

        private $qualifying;
        private $raceDistance;
        private $carPerformance;
        private $maxRacesCount;

        public static $QUALI_ONESHOT = "One shot";
        public static $QUALI_18 = "18min";
        public static $QUALI_FULL = "Q1 | Q2 | Q3";
        
        public static $RACEDISTANCE_5 = "5 rounds";
        public static $RACEDISTANCE_25 = "25%";
        public static $RACEDISTANCE_50 = "50%";

        public static $CARPERFORMANCE_REALISTIC = "Realistic";
        public static $CARPERFORMANCE_EQUAL= "Equal";

        public function __construct($qualifying, $raceDistance, $carPerformance, $maxRacesCount) {
            $this->qualifying = $qualifying;
            $this->raceDistance = $raceDistance;
            $this->carPerformance = $carPerformance;
            $this->maxRacesCount = $maxRacesCount;
        }

        public function getQualifying() {
            return $this->qualifying;
        }

        public function getRaceDistance() {
            return $this->raceDistance;
        }

        public function getCarPerformance() {
            return $this->carPerformance;
        }

        public function getmaxRacesCount() {
            return $this->maxRacesCount;
        }
    }