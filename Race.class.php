<?php
    include_once("Driver.class.php");

    class Race {

        public static $AUSTRALIA_ID = "Australia";
        public static $BAHRAIN_ID = "Bahrain";
        public static $VIETNAM_ID = "Vietnam";
        public static $SPAIN_ID = "Spain";
        public static $NETHERLANDS_ID = "Netherlands";
        public static $CHINA_ID = "China";
        public static $AUSTRIA_ID = "Austria";
        public static $CANADA_ID = "Canada";
        public static $MONACO_ID = "Monaco";
        public static $AZERBAIJAN_ID = "Azerbaijan";
        public static $BRITIAN_ID = "Great Britian";
        public static $BELGIUM_ID = "Belgium";
        public static $SINGAPORE_ID = "Singapore";
        public static $HUNGARY_ID = "Hungary";
        public static $ITALY_ID = "Italy";
        public static $ABUDHABI_ID = "Abu Dhabi";
        public static $FRANCE_ID = "France";
        public static $MEXICO_ID = "Mexico";
        public static $BRAZIL_ID = "Brazil";
        public static $USA_ID = "USA";
        public static $JAPAN_ID = "Japan";
        public static $RUSSIA_ID = "Russia";

        private $id;
        private $fastestLap;
        private $result = [];
        private $resultWithPoints = []; // key = driver id, value[0] = driver object, value[1] = points for this race

        public function __construct($id, $fastestLap, $result) {
            $this->id = $id;
            $this->fastestLap = $fastestLap;
            $this->result = $result;            
            $this->calculateResultWithPoints();
        }

        public function getId() {
            return $this->id;
        }

        public function getResultWithPoints() {
            return $this->resultWithPoints;
        }

        private function calculateResultWithPoints() {
            foreach ($this->result as $driver) {
                $this->resultWithPoints[$driver->getId()] = [ $driver, $this->getPointsForDriver($driver) ];
            }
        }

        private function getPointsForDriver($driver) {
            $points = 0;
            $place = array_search($driver, $this->result) + 1;

            switch($place) {
                case 1:
                    $points = 25;
                    break;
                case 2:
                    $points = 18;
                    break;
                case 3:
                    $points = 15;
                    break;
                case 4:
                    $points = 12;
                    break;
                case 5:
                    $points = 10;
                    break;
                case 6:
                    $points = 8;
                    break;
                case 7:
                    $points = 6;
                    break;
                case 8:
                    $points = 4;
                    break;
                case 9:
                    $points = 2;
                    break;
                case 10:
                    $points = 1;
                    break;
                default:
                    $points = 0;
                    break;
            }

            // Determine if the driver gets a point for the fastest lap
            if ($this->result[$place - 1] == $this->fastestLap && $place <= 10) {
                $points++;
            }

            return $points;
        }

        public function printAsTable($number) {
            $raceId = strtolower($this->id);
            $headlineHtml = "<h2> #$number " . $this->getId() . "</h2>";
            $content = "";

            for($i = 0; $i < sizeof($this->result); $i++) {
                $driver = $this->result[$i];                
                $classes = [];

                // Create team badge
                $team = "<div class=\"" . $driver->getTeam()->getCssClass() . "\"></div>";

                // Create the basic html
                $tmp = "<tr>";
                $tmp .= "<td>" . ($i + 1) . "</td>";
                $tmp .= "<td>" . $driver->getName() . "</td>";
                $tmp .= "<td>" . $team . "<p>" . $driver->getTeam()->getName() . "</p> </td>";
                $tmp .= "<td class=\"centered-column\">" . $this->getPointsForDriver($driver) . "</td>";
                $tmp .= "</tr>";

                // Add css to html if needed
                if ($driver->getIsPlayer()) $classes[] = "player";
                if ($driver == $this->fastestLap) $classes[] = "fastestLap";
                if (isset($classes)) {
                    $tmp = str_replace("<td>", "<td class=\"". implode(" ", $classes) . "\">", $tmp);
                    $tmp = str_replace("<td class=\"centered-column\">", "<td class=\"centered-column ". implode(" ", $classes) . "\">", $tmp);
                }

                $content .= $tmp;
            }

            echo
            "<section id=\"$raceId\">
                $headlineHtml
                <table>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Team</th>
                        <th class=\"centered-column\">Points</th>
                    </tr>
                    $content
                </table>
            </section>";
        }
    }