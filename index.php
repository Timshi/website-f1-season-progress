<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body id="index-body">
        <main>
            <div>
                <a href="2021/season1.php">
                    <img class="cover" src="img/f12021cover.png" alt="F1 2020 game cover">
                </a>
            </div>
            <div>
                <a href="2020/season1.php">
                    <img class="cover" src="img/f12020cover.png" alt="F1 2021 game cover">
                </a>
            </div>
            <div>
                <a href="acc/season1.php">
                    <img class="cover" src="img/acccover.png" alt="Assetto Corsa Competizione game cover">
                </a>
            </div>
        </main>
    </body>
</html>