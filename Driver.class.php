<?php
    include_once("Team.class.php");

    class Driver {

        private $id;
        private $name;
        private $team;
        private $isPlayer;

        public function __construct($id, $name, $team, $isPlayer = false) {
            $this->id = $id;
            $this->name = $name;
            $this->team = $team;
            $this->isPlayer = $isPlayer;
        }

        public function getId() {
            return $this->id;
        }

        public function getName() {
            return $this->name;
        }

        public function getTeam() {
            return $this->team;
        }

        public function getIsPlayer() {
            return $this->isPlayer;
        }
    }